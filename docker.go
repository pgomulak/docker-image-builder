package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"strings"

	docker "github.com/fsouza/go-dockerclient"
	log "github.com/sirupsen/logrus"
)

const (
	flagFile           = "./runonce"
	dockerDebugEnvName = "DOCKER_DEBUG"
	dockerSocket       = "unix:///var/run/docker.sock"
)

func buildImage(client *docker.Client, options map[string]*string, loginInfo docker.AuthConfigurations, buildArgs []docker.BuildArg) error {

	noCache := options["NO_CACHE"] != nil && *options["NO_CACHE"] != ""
	if noCache {
		log.Println("Layer caching is disabled")
	} else {
		log.Println("Layer caching is allowed")
	}

	name := ""
	if options["TO"] != nil {
		name = *options["TO"]
	}
	
	buildOptions := docker.BuildImageOptions{
		Name:       name,
		Dockerfile: *options["DOCKER_FILE"],
		// Always pull image even if locally accessible. This is absolutely necessary to protect
		// private source images from being reused if present already.
		Pull:         true,
		NoCache:      noCache,
		BuildArgs:    buildArgs,
		AuthConfigs:  loginInfo,
		OutputStream: os.Stdout,
		ContextDir:   *options["CONTEXT_DIR"],
	}

	//	resp, err := cli.ImageBuild(context.Background(), makeTarReader(*options["CONTEXT_DIR"]), buildOptions)
	if err := client.BuildImage(buildOptions); err != nil {
		return err
	}
	return nil
}

func pushImage(client *docker.Client, options map[string]*string, loginInfo docker.AuthConfigurations) error {

	// Obtain registry name to pushImage
	destinationImageFields := strings.Split(*options["TO"], "/")
	registry := destinationImageFields[0]

	pushOptions := docker.PushImageOptions{
		Name:         *options["TO"],
		Registry:     registry,
		OutputStream: os.Stdout,
	}

	var authInfo docker.AuthConfiguration
	if val, ok := loginInfo.Configs[registry]; ok {
		authInfo = val
	} else {
		authInfo = docker.AuthConfiguration{}
	}
	return client.PushImage(pushOptions, authInfo)
}

// Helper function to convert a string into a pointer of a string
func getPointer(s string) *string {
	return &s
}

// Retrieves a pointer to a string when envName is defined
func getEnvValue(envName string, defaultValue *string) *string {
	if val, present := os.LookupEnv(envName); present {
		return &val
	}
	return defaultValue
}

// obtainDefaults creates a map with the default values for all the variables
// that the docker build accepts
func obtainDefaults() map[string]*string {
	return map[string]*string{
		"FROM":                  nil,
		"TO":                    getEnvValue("CI_REGISTRY_IMAGE", nil),
		"CONTEXT_DIR":           getPointer("."),
		"DOCKER_FILE":           getPointer("Dockerfile"),
		"DOCKER_LOGIN_SERVER":   getEnvValue("CI_REGISTRY", nil),
		"DOCKER_LOGIN_USERNAME": getPointer("gitlab-ci-token"),
		"DOCKER_LOGIN_PASSWORD": getEnvValue("CI_BUILD_TOKEN", nil),
		"NO_CACHE":              nil,
	}
}

// This function overrides the default values if an environment variable with the same name is defined
func parseEnvVariables(defaults map[string]*string) map[string]*string {
	options := make(map[string]*string)

	// For each possible variable, see if they are set as environment variable and override them from the
	// default list
	for envName, defaultValue := range defaults {
		if envValue, present := os.LookupEnv(envName); present {
			options[envName] = &envValue
		} else {
			options[envName] = defaultValue
		}
	}

	return options
}

// Obtain a types.AuthConfig object that can be use to build using a private image
func loginWithRegistry(username, password, server *string) docker.AuthConfigurations {
	// If no username and password, ignore auth
	authConfigs := docker.AuthConfigurations{
		Configs: make(map[string]docker.AuthConfiguration),
	}

	// If no username and password, ignore auth
	if !(username == nil || password == nil || server == nil) {
		authConfigs.Configs[*server] = docker.AuthConfiguration{
			Username: *username,
			Password: *password,
		}
	}
	return authConfigs
}

//Prints useful information for the build
func printDockerBuild(options map[string]*string) {
	if options["TO"] != nil {
		log.Printf("Building Docker image: %s\n", *options["TO"])
	}
	if options["FROM"] != nil {
		log.Printf("Overriding FROM with '%s'\n", *options["FROM"])
	}
	if *options["CONTEXT_DIR"] != "." {
		log.Printf("Using Context directory in %s\n", *options["CONTEXT_DIR"])
	}
	for _, env := range os.Environ() {
		if strings.HasPrefix(env, "BUILD_ARG") {
			fmt.Printf("Using build args: '%s'\n", env)
		}
	}
}

// Override the FROM field from the Dockerfile
// This directly edits the Dockerfile and leaves it changed
func overrideFrom(dockerfile, newFrom *string) error {
	if dockerfile != nil && newFrom != nil {

		log.Printf("Overriding FROM statement with '%s'\n", *newFrom)
		input, err := ioutil.ReadFile(*dockerfile)
		if err != nil {
			return err
		}

		lines := strings.Split(string(input), "\n")

		for i, line := range lines {
			//Overrid FROM
			if strings.HasPrefix(line, "FROM ") {
				lines[i] = fmt.Sprintf("FROM %s", *newFrom)
				break
			}
		}
		output := strings.Join(lines, "\n")
		err = ioutil.WriteFile(*dockerfile, []byte(output), 0644)
		if err != nil {
			return err
		}
	}
	return nil
}

// Obtain build args from environment variables starting with 'BUILD_ARG'
func parseBuildArgs() ([]docker.BuildArg, error) {

	buildArgs := make([]docker.BuildArg, 0)
	// os.Environ returns a copy of strings representing the environment,
	// in the form "key=value".
	for _, env := range os.Environ() {
		if strings.HasPrefix(env, "BUILD_ARG") {
			comps := strings.Split(env, "=")
			if len(comps) != 3 {
				return buildArgs, fmt.Errorf("Build arg '%s' is malformed. The value should be a key value pair", env)
			}
			buildArgs = append(buildArgs, docker.BuildArg{
				Name:  comps[1],
				Value: comps[2],
			})
		}
	}
	return buildArgs, nil
}

func main() {

	// Set debug log level if DOCKER_DEBUG is set to true. Ignore otherwise.
	// When running inside gitlab-ci, we have to adapt the CONTEXT_DIR to wherever the probject is cloned
	if os.Getenv(dockerDebugEnvName) == "true" {
		log.SetLevel(log.DebugLevel)
	}

	// Abort if there is an ongoing build running. This is needed due to https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/issues/1380
	if _, err := os.Stat(flagFile); err == nil {
		log.Println("[gitlabci-docker-builder] Build already done (workaround for https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/issues/1380)")
		os.Exit(0)
	} else {
		// Create file if it does not exist
		os.OpenFile(flagFile, os.O_CREATE, 0666)
	}
	// Obtain docker build variables based on defaults and environment variables
	options := parseEnvVariables(obtainDefaults())

	printDockerBuild(options)

	// When running inside gitlab-ci, we have to adapt the CONTEXT_DIR to wherever the probject is cloned
	if ciProjectDir, present := os.LookupEnv("CI_PROJECT_DIR"); present {
		options["CONTEXT_DIR"] = getPointer(path.Join(ciProjectDir, strings.TrimPrefix(*options["CONTEXT_DIR"], "/")))
	}

	// Obtain Build args
	buildArgs, err := parseBuildArgs()
	if err != nil {
		log.Fatal(err)
	}
	// Configure login to pull the image
	loginInfo := loginWithRegistry(options["DOCKER_LOGIN_USERNAME"],
		options["DOCKER_LOGIN_PASSWORD"], options["DOCKER_LOGIN_SERVER"])

	dockerfilepath := path.Join(*options["CONTEXT_DIR"], *options["DOCKER_FILE"])

	if err := overrideFrom(&dockerfilepath, options["FROM"]); err != nil {
		log.Fatal(err)
	}

	// Allow users to override the docker host via DOCKER_HOST environment variable
	// This would allow to work with DinD in a service container for instance
	dockerHost := dockerSocket
	if env, present := os.LookupEnv("DOCKER_HOST"); present {
		dockerHost = env
	}
	client, err := docker.NewClient(dockerHost)

	// Proceed to build the image
	if err := buildImage(client, options, loginInfo, buildArgs); err != nil {
		log.Fatal(err)
	}
	log.Println("Build successfully completed!")

	// If all good building the image, push it to the registry
	if options["TO"] != nil {
		log.Printf("Pushing the built image to '%s'", *options["TO"])
		if err := pushImage(client, options, loginInfo); err != nil {
			log.Fatal(err)
		}
		log.Printf("Image successfully pushed to '%s'", *options["TO"])
	} else {
		log.Println("Do not pushing the image as the `TO` variable is not set")
	}
}
