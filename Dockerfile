FROM docker:18.09

# Add the previously built app binary to the image
COPY main  /

ENTRYPOINT ["/main"]
